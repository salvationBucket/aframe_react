import 'aframe';
import 'aframe-animation-component';
import 'aframe-particle-system-component';
import 'babel-polyfill';
import {Entity, Scene} from 'aframe-react';
import React from 'react';
import ReactDOM from 'react-dom';
import crate from './crate.jpg';
import screen from './screen.jpg';
class App extends React.Component {
  onColor(){
    this.setState({
      animation:'color'
    });
      document.getElementById('mybox').emit('color');
  }
  onScale(){
    this.setState({
      animation:'scale'
    });
    document.getElementById('mybox').emit('scale');
  }
  onRotate(){
    this.setState({
      animation:'rotate'
    });
    document.getElementById('mybox').emit('rotate');
  }
  mouseEnter(e){
    e.target.setAttribute('color','#003bab');
  }
  mouseLeave(e){
    e.target.setAttribute('color','white');
  }
  render () {
    return (
      <Scene>
        <a-assets>
          <img id="groundTexture" src="https://cdn.aframe.io/a-painter/images/floor.jpg"/>
          <img id="skyTexture" src="https://cdn.aframe.io/a-painter/images/sky.jpg"/>
          <img id="screen" src={screen}/>
          <img id="wood" src={crate}/>
        </a-assets>

        <Entity primitive="a-plane" src="#groundTexture" rotation="-90 0 0" height="100" width="100"/>
        <Entity primitive="a-sky" height="2048" radius="30" src="#skyTexture" theta-length="90" width="2048"/>
        <Entity primitive="a-plane" src="#screen" height="3" width="3" position={{x: 0, y: 4, z: -7}}
          events={{fusing: this.onColor.bind(this),mouseenter:this.mouseEnter.bind(this),mouseleave:this.mouseLeave.bind(this)}}>
          <Entity text={{value: 'Color', align: 'center',color:'white', width:'10'}}/>
        </Entity>
        <Entity primitive="a-plane" src="#screen" height="3" width="3" rotation="0 -60 0" position={{x: 5, y: 4, z: -5}}
          events={{fusing: this.onScale.bind(this),mouseenter:this.mouseEnter.bind(this),mouseleave:this.mouseLeave.bind(this)}}>
          <Entity text={{value: 'Scale', align: 'center',color:'white', width:'10'}}/>
        </Entity>
        <Entity primitive="a-plane" src="#screen" height="3" width="3" rotation="0 60 0" position={{x: -5, y: 4, z: -5}}
          events={{fusing: this.onRotate.bind(this),mouseenter:this.mouseEnter.bind(this),mouseleave:this.mouseLeave.bind(this)}}>
          <Entity text={{value: 'Rotate', align: 'center',color:'white', width:'10'}}/>
        </Entity>
          <Entity id="mybox" src="#wood" primitive="a-box" depth="1" height="1" width="1" position={{x: 0, y: 1, z: -3}}>
            <a-animation  fill="backwards" begin="scale" attribute="scale"
               dur="4000"
               from="1 1 1"
               to="0.5 0.5 0.5"/>
               <a-animation fill="backwards" begin="color" attribute="material.color" from="white" to="red" dur="5000"/>
               <a-animation  fill="backwards" begin="rotate" attribute="rotation"
                  dur="1000"
                  from="1 1 1"
                  to="0 360 0"/>
          </Entity>
        <Entity primitive="a-camera">
          <Entity primitive="a-cursor" color="red" fuse="true" fuseTimeout="1000" animation__click={{property: 'scale', startEvents: 'click', from: '0.1 0.1 0.1', to: '1 1 1', dur: 150}}/>
        </Entity>
      </Scene>
    );
  }
}

ReactDOM.render(<App/>, document.querySelector('#sceneContainer'));
